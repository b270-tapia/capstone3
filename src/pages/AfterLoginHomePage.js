import { Fragment } from 'react';
import AfterLoginBanner from '../components/AfterLoginBanner';
import AfterLoginHighlights from '../components/AfterLoginHighlights';


export default function AfterLoginHomePage() {
	
	return (
		<Fragment>
			<AfterLoginBanner />
			<AfterLoginHighlights />
		</Fragment>
	)
}