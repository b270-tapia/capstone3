import React, {useState, useEffect, useContext} from 'react'
import { useNavigate, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import {Modal, Form, Button} from 'react-bootstrap'
import {
  MDBContainer,
  MDBRow,
  MDBCard,
  MDBCardHeader,
  MDBCol,
  MDBCardBody,
  MDBTabs,
  MDBTabsItem,
  MDBTabsLink,
  MDBTabsPane,
  MDBTabsContent,
  MDBIcon,
  MDBCheckbox,
  MDBInput,
  MDBBtn,
  MDBTextArea,
} from 'mdb-react-ui-kit';
import UserContext from "../UserContext";

export default function UserProfile() {
		const { user } = useContext(UserContext);
		const {allUsers, setAllUsers} = useState([]);
		const navigate = useNavigate();
		const [userId, setUserId] = useState("");
		const [password, setPassword] = useState("");
		const [showEdit, setShowEdit] = useState(false);
		const [isActive, setIsActive] = useState(false);


	const openEdit = (id) => {
		setUserId(id)

		fetch(`${process.env.REACT_APP_API_URL}/users/${id}`, {
				headers:{
					"Authorization": `Bearer ${localStorage.getItem("token")}`
				}
			})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			
			setPassword(data.password);
			
			
		});

		setShowEdit(true)
	};

	const closeEdit = () => {

		// Clear input fields upon closing the modal
	    setPassword('');
	  
	    

		setShowEdit(false);
	};

		const fetchData = () =>{
			
			fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
				headers:{
					"Authorization": `Bearer ${localStorage.getItem("token")}`
				}
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);

			});
		};


		// to fetch all products in the first render of the page.
		useEffect(()=>{
			fetchData();
		}, [])

			const userUpdate = (e) =>{
				// Prevents page redirection via form submission
			    e.preventDefault();

			    fetch(`${process.env.REACT_APP_API_URL}/users/${userId}`, {
			    	method: "PUT",
			    	headers: {
						"Content-Type": "application/json",
						"Authorization": `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
						

					   userId: userId,
					   password: password
					    
					})
			    })
			    .then(res => res.json())
			    .then(data => {
			    	console.log(data);

			 				
			 				if(data) {

			 					localStorage.setItem('token', data.accessToken);
                    

			 								Swal.fire({
			 									title: "Success!",
			 									icon: "success",
			 									text: `You successfully change your password.`
			 								})
			 								fetchData();
			 								navigate("/user")
			 								closeEdit();
			
											
			 							} else {
			 								Swal.fire({
			 									title: "Something went wrong",
			 									icon: "error",
			 									text: "Please try again."
			 					})
			 								closeEdit();
			 				}


			    })

			    // Clear input fields
			    setPassword('');
			   
			    
		} 

	


	return(
	<section className='text-center text-md-start'>
      <div
        className='p-5 bg-image'
        style={{
          height: '300px',
          backgroundImage: 'url("https://mdbootstrap.com/img/new/textures/full/247.jpg")',
          zIndex: '-1',
        }}
      ></div>

      <div className='pb-2' style={{ backgroundColor: 'background-color: hsl(0, 0%, 98%)' }}>
        <MDBContainer>
          <MDBRow className='d-flex justify-content-center align-items-center'>
            <MDBCol lg='6' md='8' className='mb-4 mb-md-0 pt-4'>
              <img
                src='https://mdbootstrap.com/img/new/avatars/18.jpg'
                className='rounded-circle float-none float-md-start me-5 mb-3'
                alt=''
                style={{
                  width: '168px',
                  marginTop: '-110px',
                  border: '4px solid hsl(0, 0%, 98%)',
                }}
              />
              <h1 className='fw-bold'>Anna Tapia</h1>
              <p className='text-muted mx-5'>
                	Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet assumenda, commodi perferendis
                libero debitis harum ut sed nostrum minima aspernatur. .
              </p>
            </MDBCol>

            <Button variant="secondary" size="sm" className="mx-1" onClick={() => openEdit(user._id)}>Edit</Button>

    <Modal show={showEdit} fullscreen={true} onHide={closeEdit}>
    	<Form onSubmit={e => userUpdate(e)}>

    			

    <MDBContainer fluid className='mt-0'>
      <section className='text-center'>
        <div
          className='p-5 bg-image'
          style={{ backgroundImage: "url('https://t4.ftcdn.net/jpg/02/76/35/39/360_F_276353920_LYBJg7bylEGmGJJhfFP7waS1kI1jbC4m.jpg')", height: '500px' }}
        ></div>
        	
        <div
          className='card mx-4 mx-md-5 shadow-5-strong'
          style={{ marginTop: '-70px', background: 'hsla(0, 0%, 100%, 0.8)', backdropFilter: 'blur(30px)' }}
        >
          <div className='card-body py-5 px-md-5' >
           

            <MDBRow class='d-flex justify-content-center'>
              <MDBCol lg='6'  class='col-lg-6 '>
                <form>
                  <MDBInput className='mb-4'  id='password' 
    	                	label='Change Password'
    	                	type="text" 
    	                	value = {password}
    	                	onChange={e => setPassword(e.target.value)}
    	                	required
    	                 />

    	         

                  
                </form>
              </MDBCol>
            </MDBRow>
          </div>
        </div>
      </section>
    </MDBContainer>

    			<Modal.Footer>
    				

    				    <Button variant="danger" type="submit" id="submitBtn">
    				    	Save
    				    </Button>
    				
    				<Button variant="secondary" onClick={closeEdit}>
    					Close
    				</Button>
    			</Modal.Footer>

    		</Form>	
    	</Modal>
          </MDBRow>
        </MDBContainer>
      </div>
    </section>
	)
}