import React, {useContext, useEffect, useState} from "react";

import { Button, Table, Modal, Form } from "react-bootstrap";
import {Navigate} from "react-router-dom";
import Swal from "sweetalert2";
import Cart from '../components/cart/Cart';
import UserContext from "../UserContext";
import {
  NavbarBrand,

  ModalHeader,
  ModalBody
} from 'reactstrap';
import {
  MDBContainer,
  MDBRow,
  MDBCard,
  MDBCardHeader,
  MDBCol,
  MDBCardBody,
  MDBTabs,
  MDBTabsItem,
  MDBTabsLink,
  MDBTabsPane,
  MDBTabsContent,
  MDBIcon,
  MDBCheckbox,
  MDBInput,
  MDBBtn,
  MDBTextArea,
} from 'mdb-react-ui-kit';

export default function AdminDashboard(){

	const { user } = useContext(UserContext);

	// Create allProducts state to contain the courses from the response of our fetch data.
	const [allProducts, setAllProducts] = useState([]);
	const [allOrders, setAllOrders] = useState([]);
	// const [allOrders, setAllOrders] = useState([]);

	// State hooks to store the values of the input fields for our modal.
	const [productId, setProductId] = useState("");
	const [productImage, setProductImage] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [size, setSize] = useState("");
	const [price, setPrice] = useState(0);
	 const [modal, setModal] = useState(false);
	const toggleModal = () => setModal(!modal);
   

    // State to determine whether submit button in the modal is enabled or not
    const [isActive, setIsActive] = useState(false);

    // State for Add/Edit Modal
    const [showAdd, setShowAdd] = useState(false);
	const [showEdit, setShowEdit] = useState(false);

	// To control the add Product modal pop out
	const openAdd = () => setShowAdd(true); //Will show the modal
	const closeAdd = () => setShowAdd(false); //Will hide the modal



	// To control the edit Product modal pop out
	// We have passed a parameter from the edit button so we can retrieve a specific Product and bind it with our input fields.
	const openEdit = (id) => {
		setProductId(id);

		// Getting a specific Product to pass on the edit modal
		fetch(`${ process.env.REACT_APP_API_URL }/products/${id}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			// updating the Product states for editing
			setName(data.name);
			setDescription(data.description);
			setSize(data.size);
			setPrice(data.price);
			
		});

		setShowEdit(true)
	};

	const closeEdit = () => {

		// Clear input fields upon closing the modal
	    setName('');
	    setDescription('');
	    setSize('');
	    setPrice(0);
	    

		setShowEdit(false);
	};


	// [SECTION] To view all Product in the database (active & inactive)
	// fetchData() function to get all the active/inactive products.
	const fetchData = () =>{
		// get all the products from the database
		fetch(`${process.env.REACT_APP_API_URL}/products/all`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllProducts(data.map(product => {
				return (
					<tr key={product._id}>
						<td>{product._id}</td>
						<td>
							<img src={`http://localhost:4000/${product.productImage}`}
                        alt="Uploaded Image" />
             </td>           
						<td>{product.name}</td>
						<td>{product.description}</td>
						<td>{product.size}</td>
						<td>{product.price}</td>
						
						<td>{product.isActive ? "Active" : "Inactive"}</td>
						<td>
							{
								// conditonal rendering on what button should be visible base on the status of the product
								(product.isActive)
								?
									<Button variant="danger" size="sm" onClick={() => archive(product._id, product.name)}>Archive</Button>
								:
									<>
										<Button variant="success" size="sm" className="mx-1" onClick={() => unarchive(product._id, product.name)}>Unarchive</Button>
										<Button variant="secondary" size="sm" className="mx-1" onClick={() => openEdit(product._id)}>Edit</Button>
									</>

							}
						</td>
					</tr>
				)
			}));
		});
	};

	// const getAllOrders = () =>{
	// 	// get all the products from the database
	// 	fetch(`${process.env.REACT_APP_API_URL}/users/allOrders`, {
	// 		headers:{
	// 			"Authorization": `Bearer ${localStorage.getItem("token")}`
	// 		}
	// 	})
	// 	.then(res => res.json())
	// 	.then(data => {
	// 		console.log(data);

	// 		setAllOrders(data.map(product => {
	// 			return (
	// 				<tr key={product._id}>
	// 					<td>{product._id}</td>
	// 					<td>
	// 						<img src={`http://localhost:4000/${product.productImage}`}
  //                       alt="Uploaded Image" />
  //            </td>           
	// 					<td>{product.name}</td>
	// 					<td>{product.quantity}</td>
						
	// 				</tr>
	// 			)
	// 		}));
	// 	});
	// };

	// to fetch all products in the first render of the page.
	useEffect(()=>{
		fetchData();
	}, [])


	// [SECTION] Setting the Product to Active/Inactive

	// Making the Product inactive
	const archive = (id, productName) =>{
		console.log(id);
		console.log(productName);

		// Using the fetch method to set the isActive property of the product document to false
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}/archive`, {
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Archive Successful",
					icon: "success",
					text: `${productName} is now inactive.`
				});
				// To show the update with the specific operation intiated.
				fetchData();
			}
			else{
				Swal.fire({
					title: "Archive unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}

	// Making the product active
	const unarchive = (id, productName) =>{
		console.log(id);
		console.log(productName);

		// Using the fetch method to set the isActive property of the product document to false
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}/archive`, {
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: "Unarchive Successful",
					icon: "success",
					text: `${productName} is now active.`
				});
				// To show the update with the specific operation intiated.
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				});
			}
		})
	}

	// [SECTION] Adding a new product
	// Inserting a new product in our database
	const addProduct = (e) =>{
			// Prevents page redirection via form submission
		    e.preventDefault();
		    

		    fetch(`${process.env.REACT_APP_API_URL}/products/`, {
		    	method: "POST",
		    	headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
						productImage: productImage,
				    name: name,
				    description: description,
				    size: size,
				    price: price
				    
				})
		    })
		    .then(res => res.json())
		    .then(data => {
		    	console.log(data);

		    	if(!data){
		    		Swal.fire({
		    		    title: "Product succesfully Added",
		    		    icon: "success",
		    		    text: `${name} is now added`
		    		});

		    		// To automatically add the update in the page
		    		fetchData();
		    		// Automatically closed the modal
		    		closeAdd();
		    	}
		    	else{
		    		Swal.fire({
		    		    title: "Error!",
		    		    icon: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});
		    		closeAdd();
		    	}

		    })

		    // Clear input fields
		    setName('');
		    setDescription('');
		    setSize('');
		    setPrice(0);
		    
	}



	// [SECTION] Edit a specific Product
	// Updating a specific Product in our database
	// edit a specific Product
	const editProduct = (e) =>{
			// Prevents page redirection via form submission
		    e.preventDefault();

		    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
		    	method: "PUT",
		    	headers: {
					"Content-Type": "application/json",
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({

				    name: name,
				    description: description,
				    size: size,
				    price: price,
				    
				})
		    })
		    .then(res => res.json())
		    .then(data => {
		    	console.log(data);

		    	if(data){
		    		Swal.fire({
		    		    title: "Product succesfully Updated",
		    		    icon: "success",
		    		    text: `${name} is now updated`
		    		});

		    		// To automatically add the update in the page
		    		fetchData();
		    		// Automatically closed the form
		    		closeEdit();

		    	}
		    	else{
		    		Swal.fire({
		    		    title: "Error!",
		    		    icon: "error",
		    		    text: `Something went wrong. Please try again later!`
		    		});

		    		closeEdit();
		    	}

		    })

		    // Clear input fields
		    setName('');
		    setDescription('');
		    setSize('');
		    setPrice(0);
		    
	} 

	// Submit button validation for add/edit Product
	useEffect(() => {

        // Validation to enable submit button when all fields are populated and set a price and slot greater than zero.
        if(name != "" && description != "" && price > 0 && size != ""){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [name, description,size, price ]);

	// [Order Section]
	// const orders = () =>{
	// 	// get all the orders from the database
	// 	fetch(`${process.env.REACT_APP_API_URL}/users/allOrders`, {
	// 		headers:{
	// 			"Authorization": `Bearer ${localStorage.getItem("token")}`
	// 		}
	// 	})
	// 	.then(res => res.json())
	// 	.then(data => {
	// 		console.log(data);

	// 		setAllOrders(data.map(order => {
	// 			return (
	// 				<tr key={order._id}>
	// 					<td>{order._id}</td>
	// 					{/*<td>{usr.order.id}</td>*/}
	// 				</tr>
	// 			)
	// 		}));
	// 	});
	// }

   

	return(
		(user.isAdmin)
		?
		<>
			{/*Header for the admin dashboard and functionality for create Product and show enrollments*/}
			<div className="mt-5 mb-3 text-center">
				<h1>Admin Dashboard</h1>
				{/*Adding a new Product */}
				<Button variant="success" className="
				mx-2" onClick={openAdd}>Add Product</Button>
				{/*To view all the user enrollments*/}
				<Button variant="secondary" className="
				mx-2" ><a href="/order">Show Orders </a></Button>        		      
	      <Modal isOpen={modal} toggle={toggleModal}>
	      <ModalHeader toggle={toggleModal}>Cart</ModalHeader>
	        <ModalBody>
	          <Cart />
	        </ModalBody>
	      </Modal>
				{/*onClick={() => orders(order._id)} */}
			</div>
			{/*End of admin dashboard header*/}
			{/*Start of Show Order Section*/}
		
			{/*End of Show Order Section*/}
			{/*For view all the products in the database.*/}
			<Table striped bordered hover>
		      <thead>
		        <tr>
		          <th>Product ID</th>
		          <th>Product Image</th>

		          <th>Product Name</th>
		          <th>Description</th>
		          <th>Size</th>
		          <th>Price</th>		
		          <th>Status</th>
		          <th>Actions</th>
		        </tr>
		      </thead>
		      <tbody>
	        	{allProducts}
		      </tbody>
		    </Table>
			{/*End of table for Product viewing*/}

	    	{/*Modal for Adding a new Product*/}
	        <Modal show={showAdd} fullscreen={true} onHide={closeAdd}>
	    		<Form onSubmit={e => addProduct(e)}>

	    			<Modal.Header closeButton>
	    				<Modal.Title className="ms-5">Add New Product</Modal.Title>
	    			</Modal.Header>

	    			<MDBContainer fluid className='mt-0'>
      <section className='text-center'>
        <div
          className='p-5 bg-image'
          style={{ backgroundImage: "url('https://t3.ftcdn.net/jpg/01/29/93/68/360_F_129936834_Bfn9q5eyPwwXpP2MexPbIkGekW60U3tW.jpg')", height: '300px' }}
        ></div>
        	{/*https://image.shutterstock.com/image-photo/set-paper-take-away-cups-260nw-1968819760.jpg*/}
        <div
          className='card mx-4 mx-md-5 shadow-5-strong'
          style={{ marginTop: '-100px', background: 'hsla(0, 0%, 100%, 0.8)', backdropFilter: 'blur(30px)' }}
        >
          <div className='card-body py-5 px-md-5'>
           

            <MDBRow class='d-flex justify-content-center'>
              <MDBCol lg='6' class='col-lg-6'>
                <form>
                  <MDBInput className='mb-4'  id='name' 
    	                	label='Product Name'
    	                	type="text" 
    	                	value = {name}
    	                	onChange={e => setName(e.target.value)}
    	                	required
    	                 />


    	                 <MDBTextArea className='mb-3'  id='description' 
    	                 	label='Product Description'
    	                 	type="text"
    	                 	as="textarea" 
    	                 	rows={4}
    	                 	value = {description}
    	                 	onChange={e => setDescription(e.target.value)}
    	                 	required
    	                  />
    	         
    	                  <MDBInput className='mb-3'  id='size' 
    	                  	label='Product Size'
    	                  	type="text" 
    	                  	value = {size}
    	                  	onChange={e => setSize(e.target.value)}
    	                  	required
    	                   />

    	                   <MDBInput className='mb-3'  id='price' 
    	                   	label='Product Price'
    	                   	type="number" 
    	                   	value = {price}
    	                   	onChange={e => setPrice(e.target.value)}
    	                   	required
    	                    />

    	                     <MDBInput className='mb-3'  id='productImage' 
    	                   	// label='Product Image'
    	                   	type="file" 
    	                   	value = {productImage}
    	                   	onChange={e => setProductImage(e.taget.value)}
    	                   	required
    	                    />

                  
                </form>
              </MDBCol>
            </MDBRow>
          </div>
        </div>
      </section>
    </MDBContainer>

	    			<Modal.Footer>
	    				{ isActive 
	    					? 
	    					<Button variant="primary" type="submit" id="submitBtn">
	    						Save
	    					</Button>
	    				    : 
	    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
	    				    	Save
	    				    </Button>
	    				}
	    				<Button variant="secondary" onClick={closeAdd}>
	    					Close
	    				</Button>
	    			</Modal.Footer>

	    		</Form>	
	    	</Modal>
	    {/*End of modal for adding product*/}

    	{/*Modal for Editing a product*/}
        <Modal show={showEdit} fullscreen={true} onHide={closeEdit}>
    		<Form onSubmit={e => editProduct(e)}>

    			<Modal.Header closeButton>
    				<Modal.Title className="ms-5">Edit a Product</Modal.Title>
    			</Modal.Header>

    			<MDBContainer fluid className='mt-0'>
      <section className='text-center'>
        <div
          className='p-5 bg-image'
          style={{ backgroundImage: "url('https://image.shutterstock.com/image-photo/set-paper-take-away-cups-260nw-1968819760.jpg')", height: '350px' }}
        ></div>
        	
        <div
          className='card mx-4 mx-md-5 shadow-5-strong'
          style={{ marginTop: '-100px', background: 'hsla(0, 0%, 100%, 0.8)', backdropFilter: 'blur(30px)' }}
        >
          <div className='card-body py-5 px-md-5' >
           

            <MDBRow class='d-flex justify-content-center'>
              <MDBCol lg='6'  class='col-lg-6 '>
                <form>
                  <MDBInput className='mb-4'  id='name' 
    	                	label='Product Name'
    	                	type="text" 
    	                	value = {name}
    	                	onChange={e => setName(e.target.value)}
    	                	required
    	                 />


    	                 <MDBTextArea className='mb-3'  id='description' 
    	                 	label='Product Description'
    	                 	type="text"
    	                 	as="textarea" 
    	                 	rows={4}
    	                 	value = {description}
    	                 	onChange={e => setDescription(e.target.value)}
    	                 	required
    	                  />
    	         
    	                  <MDBInput className='mb-3'  id='size' 
    	                  	label='Product Size'
    	                  	type="text" 
    	                  	value = {size}
    	                  	onChange={e => setSize(e.target.value)}
    	                  	required
    	                   />

    	                   <MDBInput className='mb-3'  id='price' 
    	                   	label='Product Price'
    	                   	type="number" 
    	                   	value = {price}
    	                   	onChange={e => setPrice(e.target.value)}
    	                   	required
    	                    />

                  
                </form>
              </MDBCol>
            </MDBRow>
          </div>
        </div>
      </section>
    </MDBContainer>

    			<Modal.Footer>
    				{ isActive 
    					? 
    					<Button variant="primary" type="submit" id="submitBtn">
    						Save
    					</Button>
    				    : 
    				    <Button variant="danger" type="submit" id="submitBtn" disabled>
    				    	Save
    				    </Button>
    				}
    				<Button variant="secondary" onClick={closeEdit}>
    					Close
    				</Button>
    			</Modal.Footer>

    		</Form>	
    	</Modal>
    	{/*End of modal for editing a product*/}
		</>
		:
		<Navigate to="/admin" />
	)
}