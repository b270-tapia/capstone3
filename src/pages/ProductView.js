import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import {
  MDBContainer,
  MDBRow,
  MDBCard,
  MDBCardHeader,
  MDBCol,
  MDBCardBody,
  MDBTabs,
  MDBTabsItem,
  MDBTabsLink,
  MDBTabsPane,
  MDBTabsContent,
  MDBIcon,
  MDBCheckbox,
  MDBInput,
  MDBBtn,
  MDBTextArea,
} from 'mdb-react-ui-kit';


export default function ProductView() {

	const navigate = useNavigate();
	// To be able to obtain user ID so we can enroll a user
	const { user } = useContext(UserContext);

	// The "useParams" hook allows us to retrieve the productId passed via the URL
	const { productId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [productImage, setProductImage] = useState("");
	const [size, setSize] = useState("");
	const [price, setPrice] = useState(0);

	const order = (productId) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/order`, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				
			})
		})
		.then(res=> res.json())
		.then(data => {
			console.log(data);

			if(data) {
				Swal.fire({
					title: "Successfully purchased!",
					icon: "success",
					text: "You have successfully purchased this product."
				})

				navigate("/products");
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	useEffect(() => {

		console.log(productId)
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProductImage(data.productImage)
			setName(data.name);
			setDescription(data.description);
			setSize(data.size);
			setPrice(data.price);
		})

	}, [productId])


	return(
		
	<Container className="ps-5">

		<section style={{ width: '45rem'}} className=" p-1 m-5 text-center ">
		  
		    <MDBRow className='g-0 align-items-center'>
		      <MDBCol  className='mb-5 mb-lg-0'>
		        <div
		          className='card cascading-right'
		          style={{ background: 'hsla(0, 0%, 100%, 0.55)', backdropFilter: 'blur(30px)' }}
		        >
		          <div className='card-body p-0 shadow-5'>
		            
		                <Card.Body className="mb-3 py-0">                                     
		                    <Card.Title className="pb-3">{name}</Card.Title>
		                    <Card.Subtitle >Description:</Card.Subtitle>
		                    <Card.Text >{description}</Card.Text>
		                     <Card.Subtitle>Size:</Card.Subtitle>
		                    <Card.Text>{size}</Card.Text>
		                    <Card.Subtitle>Price:</Card.Subtitle>
		                    <Card.Text>PhP {price}</Card.Text>

		                     {user.id !== null
					        	?
					        	<Button variant="primary" onClick={() => order(productId)}>Check Out</Button>
					        	:
					        	<Button variant="danger" as={Link} to="/">Log in to Order</Button>
					        }

		                </Card.Body>
		                                           
		          </div>
		        </div>
		      </MDBCol>

		      <MDBCol lg='6' className='mb-5 mb-lg-0'>
		        <img className="pb-3" src={`${process.env.REACT_APP_API_URL}/${productImage}`}
		                    alt="Uploaded Image" />
		      </MDBCol>
		    </MDBRow>
		  
		</section>
		
	</Container>
	)
}