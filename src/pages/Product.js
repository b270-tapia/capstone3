import React from 'react';
import {Col} from 'react-bootstrap'
import { Fragment, useEffect, useState } from 'react';
// import coursesData from '../data/coursesData';
import ProductCard from '../components/ProductCard';

export default function Products() {
	// Checks to see if the mock data was captured
	// console.log(coursesData);

	// State that will be used to store the courses retrieved from the database
	
	const [products, setProducts] = useState([]);

	// const [cartItems, setCartItems] = useState([]);

	// const onAdd = (product) => {
	//     const exist = cartItems.find((x) => x.id === product.id);
	//     if (exist) {
	//       setCartItems(
	//         cartItems.map((x) =>
	//           x.id === product.id ? { ...exist, qty: exist.qty + 1 } : x
	//         )
	//       );
	//     } else {
	//       setCartItems([...cartItems, { ...product, qty: 1 }]);
	//     }
	//   };
	//   const onRemove = (product) => {
	//     const exist = cartItems.find((x) => x.id === product.id);
	//     if (exist.qty === 1) {
	//       setCartItems(cartItems.filter((x) => x.id !== product.id));
	//     } else {
	//       setCartItems(
	//         cartItems.map((x) =>
	//           x.id === product.id ? { ...exist, qty: exist.qty - 1 } : x
	//         )
	//       );
	//     }
	//   };


	// "map" method loops through the individual course in our mock database and returns a CourseCard component for each course
	// Multiple components created through the map method must have a unique key that will help React JS identify which components/elements have been changed, added or removed
	// Everytime the map method loops through the data, it creates a "CourseCard" component and then passes the current element in our coursesData array using the courseProp



	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/allActive`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setProducts(data.map(product => {
				return(
				
					<ProductCard key={product.id} productProp={product}/>  //onAdd={onAdd}
				
				)
			}))
		})

	}, [])
	
	return (

		<Fragment>
			{/*<Highlights />*/}
			<h1 className="pt-3 ms-5">Products</h1>
			{products}
			
		</Fragment>
	)
}