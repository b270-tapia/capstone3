import { Row, Col, Card, Carousel } from 'react-bootstrap';
import Typewriter from 'typewriter-effect';
import {
  MDBFooter,
  MDBContainer,
  MDBCol,
  MDBRow,
  MDBIcon,
  MDBRipple
} from 'mdb-react-ui-kit';



export default function AfterLoginHighlights() {
	return (






		<MDBFooter  className='pt-0   text-center text-black' style={{ backgroundColor: 'white' }}>

      
        <section className="d-flex ">

        

            

              

            
                 <MDBCol  lg='7' className="mr-2">

              <div className='ratio ratio-16x9'>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/lAd3LYIZMjs?autoplay=1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
              </div>
            </MDBCol>  
             <MDBCol lg='5' className="pl-5 pt-0">

              
               
              <img src="https://i.postimg.cc/DyHMY9w9/ezgif-com-resize.webp">
              </img>
          </MDBCol> 
      
        </section>
        <MDBRow className="p-5">
            <MDBCol lg='2' md='12' className='mb-4 mb-md-0'>
              <MDBRipple
                rippleColor='light'
                className='bg-image hover-overlay hover-zoom shadow-1-strong rounded'
              >
                <img src='https://i.postimg.cc/D0QgzNTz/ezgif-com-resize-1.jpg' className='w-100' />
                <a href='#!'>
                  <div
                    className='mask'
                    style={{ backgroundColor: 'rgba(251, 251, 251, 0.2)' }}
                  ></div>
                </a>
              </MDBRipple>
            </MDBCol>
            <MDBCol lg='2' md='12' className='mb-4 mb-md-0'>
              <MDBRipple
                rippleColor='light'
                className='bg-image hover-overlay hover-zoom shadow-1-strong rounded'
              >
                <img src='https://i.postimg.cc/nh9XYDX4/ezgif-com-resize-2.jpg' className='w-100' />
                <a href='#!'>
                  <div
                    className='mask'
                    style={{ backgroundColor: 'rgba(251, 251, 251, 0.2)' }}
                  ></div>
                </a>
              </MDBRipple>
            </MDBCol>
            <MDBCol lg='2' md='12' className='mb-4 mb-md-0'>
              <MDBRipple
                rippleColor='light'
                className='bg-image hover-overlay hover-zoom shadow-1-strong rounded'
              >
                <img src='https://i.postimg.cc/9ftZZ7kx/ezgif-com-resize-6.jpg' className='w-100' />
                <a href='#!'>
                  <div
                    className='mask'
                    style={{ backgroundColor: 'rgba(251, 251, 251, 0.2)' }}
                  ></div>
                </a>
              </MDBRipple>
            </MDBCol>
            <MDBCol lg='2' md='12' className='mb-4 mb-md-0'>
              <MDBRipple
                rippleColor='light'
                className='bg-image hover-overlay hover-zoom shadow-1-strong rounded'
              >
                <img src='https://i.postimg.cc/XYg9y5ms/ezgif-com-resize-5.jpg' className='w-100' />
                <a href='#!'>
                  <div
                    className='mask'
                    style={{ backgroundColor: 'rgba(251, 251, 251, 0.2)' }}
                  ></div>
                </a>
              </MDBRipple>
            </MDBCol>
            <MDBCol lg='2' md='12' className='mb-4 mb-md-0'>
              <MDBRipple
                rippleColor='light'
                className='bg-image hover-overlay hover-zoom shadow-1-strong rounded'
              >
                <img src='https://i.postimg.cc/c4y39TND/ezgif-com-resize-4.jpg' className='w-100' />
                <a href='#!'>
                  <div
                    className='mask'
                    style={{ backgroundColor: 'rgba(251, 251, 251, 0.2)' }}
                  ></div>
                </a>
              </MDBRipple>
            </MDBCol>
            <MDBCol lg='2' md='12' className='mb-4 mb-md-0'>
              <MDBRipple
                rippleColor='light'
                className='bg-image hover-overlay hover-zoom shadow-1-strong rounded'
              >
                <img src='https://i.postimg.cc/tRPZxPVF/ezgif-com-resize-3.jpg' className='w-100' />
                <a href='#!'>
                  <div
                    className='mask'
                    style={{ backgroundColor: 'rgba(251, 251, 251, 0.2)' }}
                  ></div>
                </a>
              </MDBRipple>
            </MDBCol>
          </MDBRow>
     

      <section className=' mt-3 border-top'>
              <MDBContainer  className='text-center text-md-start mt-5  p-0 '>
                <MDBRow className='mt-0 mb-0'>
                  <MDBCol md="3" lg="4" xl="3" className='mx-auto mb-4'>
                    <h6 className='text-uppercase fw-bold mb-0'>
                      <MDBIcon icon="gem" className="me-0" />
                      Company name
                    </h6>
                    <p>
                    <small className='fw-bold ms-auto '>
                <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCAD2APoDAREAAhEBAxEB/8QAHQABAAICAwEBAAAAAAAAAAAAAAcIBQYBBAkDAv/EAE4QAAEEAQMCAwMHBgkICwEAAAEAAgMEBQYHERIhCDFBExRRCSIyQmFx0hUYVoGRlBYXI1KCkqGxwSQzNUNicoPDJSY2RUZTVXOTsrPT/8QAGwEBAAIDAQEAAAAAAAAAAAAAAAMFAQQGAgf/xAA4EQEAAgECBAMFBgUEAwEAAAAAAQIDBBEFEiExBkFRExRhcZEWIjKhsdEzQlOB8BVDweEkNFJy/9oADAMBAAIRAxEAPwD1TQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEHHI+KByD5FBygICAgICAgICAgICAgICAgICAgIOveyFHGVJb2RtxVq0DeuWWV4YxjfiSewXjJkrirNrztEMxG/ZDmqPFPonETPrafoXc49h/zkZEEB+5zvnH7w3hc9qfEumwzy4om/wCUJ66a9o3no1CXxeZbrPstEU2t9A+88n+xir58V28scfVNGjj1fj87zNn/AMF0P3x/4Vj7V5J7Y4Z9zj1PzvM3+hdD98f+FPtXfzxwe5x6n53mb/Quh++P/Cn2rvt/Dg9zj1b9s5vjkN0M9dw9vAV6DalP3kPisOkLj1tbxwQPirXhHGr8Sy2x2pttG6DNg9lETEpfXQNdr2utf6O2103a1brnUNPDYqp2ks2n8DqPkxoHznvPHZrQXH0C82vFY3lJixXzW5Mcbyoput8qHkXW5sdszoiuKreWsyme6i+T/aZWjI6R8Ot/PxaFqZNX5Uh0em8PdN9Rbr6R+6Dr/j78VN6Uyt3ErVGk9o62GqNa37PnMcf2kqGdTknzWMcF0Ufy/nLJYL5RLxP4iwyW/qPB5qJpHMN7DRNDgPTqh9m4LManJHd5vwLR27RMf3WC2x+VC0xkpIsfu3oW1hXuPDsjh3m3XBJ83Qu4laP90vU1NXX+aFXqPD+SvXDbf4T0XK0TrzR+4uAr6n0PqOhmsXZ/zdmnKHt59Wu9WuHq1wBHqFtVtF43hQ5cWTDbkyRtLPr0jEESbzb2ZDa7LY7G08BXvi9WfO58th0fSWv6eAADyqDi/GbcMtWta77p8OD228o8/O8zn6FUP31/4VUfavJHfFH1Txo4nzPzvM7+hND99f8AhT7WX8scfVn3L4n53md/Qmh++v8Awp9q8nnij6nucer9R+L3MB49toim5vqG3ng/2sWY8V33/hfmx7nt5tu0z4rNFZWdtbUOMvYV7yAJTxYhH3lg6h/VVjpvEumyzy5vuz9YRW0to616plx2SoZenDkcZdht1Z29cU0Lw9jx8QR2K6HHlpmrF8c7xPo15iY6S7K9sCAgICAgICDqZbKUcLjbOWydllepTidNNK89mMaOSVHmy0wUnJedojuzEbztClW7G7Ob3Lyz+qWSthYHn3SiDwOPSST+c8/sb5D1J+b8V4nk4hk7/cjtH/Kzw4Yxx17tCBAPJIHPxVTunbdh9ptyc/Xbbxmi8nJA8cskkjELXD4j2haSPuW/h4VrM9ebHjnb6fqinNSOkyyP8Qu7fpouyP8Ajw/jU8cC4hH+3+cPPvGP1fKzsXuxVgksy6LtlkTS93RLE53A8+AHcn7gsTwPXxE2nH+cHvGP1aJxz34PZVU1ms7TCaOvVOXhJH/XjM8/+lf85i6jwr/7N/8A8/8ALV1f4I+a1E73Rxue1rnFoJ6W8cu+wc9uV3MztCv23eMnii3w3A3p3LyUutK1vE1MJbmpY7ATct/JjWu6XB7fWc8cvee58h80BVme05J2l3vDNJh02DfHO8z3lDfHCgiNljvuIwckIHJQb5s5vduHsVqhmqNAZh1dz3N99oy8uqX4x9SaPnv254cOHN8wQvdclsc71auq0WHWV5csf384euXh28QekfEPohuqNO81L9Vwgy2LlkDpqM5HPBI+lG7uWP44cAfIggWeLLGWN4cPrNHk0WTkv/afVKqlairPi5/7Uaf4H/d8v/6hcP4s/i4/lP6t7Rz92UEd/gVyndux0bzT2O3XvVYrlfRdv2UzQ9nXJExxB8iWucCP1hWePguuyVi9cc7T8kU6jH2mX2/iD3d/Qyx+8Q/jXueBcQ/p/nH7sRqMcebH5baDc3B1328jovJNhZ3c+JjZg0fE+zLjx9qiycI1uGvNbHO31Z9tS3TdqHB8x8VXzvHdJ0bztXupnds8w2avJJZxNh498ol/zXj1eweTXgeo8/I/ZacL4pk4fk333pPeEeXFXLXaO662FzOO1BiquaxNltipdibNDI3yc0/3H0I9CCvpWHNTPSMlJ3ieqqtE1nll3lIwICAgICAgr74sNYzUsZjNEVJi05BxuXAD5wsPDGn7C/k/0FynijVTXFXT1nv1n5NrS4+aZmVZOftXDxCx281l/Dns7jm4yvuDqWkyxZtfPxsEzeWQx89pi0+b3ebefIcHzPbtvD/CKRSNVnjeZ7R6K/UZ+aeWqwnAPbjuutae+4Gt+AWNoNjpbwekBJiJIjZHsmwG0csjpZNHxF8ji9x95m7knk/X+Kqp4JobTNrY+/xlNGoyR0iWY0pthofQ1+bJaXwbKNieL2Ej2zSP6mcg8cOcR5gKfS8O02ivN8Ndpl5vlveNrSgzx5b3at2U0HpTK6HyXueWt6khd87vHNXgifJJDIPrMeehrh58HtwVNqck0iOVZ8H0lNXktXJ22/yWmX9r9iPlA9Ex7maZtu0vrmvEyrk5IWtklgsBvzYrcXb28fY+zlBa4t8ndi0YmtNRXeGxXPqeC5PZX60nt/0qLuj4KfEHtdPPJY0VNqLFxckZLAg24y0fWdEB7Vn28t4+0rVvp707RuvdPxfS6jaObafSf3QZPDJWnfVsxPhnjPD4pWFkjT8C08EKHaVjWYvG8dnz8vNYZEBBJXh63uzmwO5mO11ijJLR5FbMUmuIFyi5w9ozj+e36bD6OaPQlSYr+ytu09fpI1uH2c9/L5vanAZzF6lwlDUOEust4/JVo7dWxGeWyxSNDmOH3ghWvNzRvDgL1mlprbvDDas2y0Trm1Xu6pwjL01WMxROdNIzpaTyR81w9Vp6nQ6fWbTnrvMM0yXx/glg2+HzaJjg5uj4gWnkH3qfsf661o4LoIneMf6vfvGSY2mUhNY1gDeFa1rshc/N+A/YvTG7kAefCxtDKA/ETs9j7eJta+05SZBfpj2uQiibw2xD9aTgdg9vmT6jnnuAuW4/wmmXHOpwxtaO/wAY/ds6fNNZ5J7KvEk+R9Vw0xt0WULKeE7WMs1fKaGtSkisPf6YJ+ixzumVo+zqLXf0iu18MavmrbT28usNDV0688LFLrmmICAgICDhYkU08S959vdq/C93LadWrAzv5Ax9Z/teV878R5ObX2ifKIWemjbHujOpWN23BSaeDYlZCD/vODf8VS46xa8Vnz6NiZ2rL0Px1KvjaFbHVWBkNWJkEbR5BrQAB+wL63jpGOsUr2hSzO87uwpGHKDhBygIKSfKWYaPOs2kx9yV8FG5qeWjZmb5xtmbE0uHPbkN6iPuWnq435YX/ArcntbR35VK8TqjdDwmb15aHT+QNHNaevzY63DI0ur5Cu1/LWSs5HXG9nQ8HsR1BzSD3WtzWwX6OitTBxXTRv2n6xL0t8O3jK2w33p1sXJch09q8tAmwlycAyv9TWkPAmafgOHj1b6nexZ65PPq5DW8Lz6Kd5jevrCV9Y7W7b7hRGHW2hcDnARxzfx8cz2/c4jqH6ipJpWe8NPHqMuHrjtMf3QdrD5Ozw16nL5MXgsrpmZw7OxGReGB3x9nN7Rn6gAop0+Oywxca1mL+bf5wr9r75LfVmPimtba7lUMt0gujpZiqasrv9kTRlzCfvY0fcobaTb8K0w+IaT0zV2+Sneu9vda7Y6hn0rr7Td3CZSDuYLLOBIzngPjeOWyMPo5pI/X2WrbHak7SvcOoxaisWxW3a6vCbbd6kfJqbkzas2UuaIvzuktaLyBqw9R5Ipzgywj7mu9q0fY0BWWmtzU2cbx7BGLVc8fzRut2tlSCAgICD4260FytLUsxh8U7HRSNPk5rhwR+wrzasWiaz2lmJ26vPDKUjjcrexpJPudmWv/AFHlv+C+R5ccYstqR5TMLis81YlIXhzvSUt3MQ1p+bajsVnj4gxOd/ewK28PX5NfWPXf9EOojfFK6a+kKwQEBAQEBJFM/EvRfT3avzOb0tu1Ks7O3n8zoP8Aawr5z4jpy6+0+sR+iz00744RlUsmlbgujzrysm/quDv8FSUtyXi8+U7p7RvWXohjb1bJ0K2SpyB8FuJk8Th5Oa9ocD+wr67ivGWkXr2lSz0nZ2VICAgICCsfyhmhrmqvDxcz2Lb/AJdo/I1s7G9o+cyNhMcrh/utk6/6ChzxvTePJbcFzxh1UVntbor74m9uK/iS2S034tNu6nt81WxTINVUYGh0kjYR0yydI7l8Dw/n1MRB+qOdbJWMtfaQs+H550OptosnaZ6f58VHWP4LZI3EOaQ5rgeCCPIg+h+BWp1js6RPe1fjg8Qm1kMONi1THqTFQ8BtHPMNnpaPqsm5EzR8B1ED4KamovToq9TwfS6jrttPrH7dlo9B/Kj6FvMirbj7eZjDTcAPs4uVl6Dn49DuiQD7OHFbNdVWY+8p8vh7LX+FaJ+fRZfbLxH7K7vvFbQWv8ZfukAmhI417g7f+TKGvP3gEKeuSt+0qjPotRp+uSsxD8797DaK3/0RPpTVVZsdmMOkxeTjYDPjrHHaRh9WnsHsPZw7efBGL4oydzSavJpMkXpPzj1eN+5O3eptqdb5XQGr6ggyeJm9lIW8+zmYRyyWMnzY9pDmn4Hg9wVWZKTS20u90+orqsUZadpW2+SxyM0e5GuMQ0u9jYwdWy4enVHYLR/ZI5bGkn70wo/EUROOlvPd6TqwcoICAgIPjcsw0qstyy8MhgY6WRx8mtaOSf2BeMl4x1m09oZiN52eeGTu/lPK3cnwf8ssy2O/+28u/wAV8ky39pltf1mVxWOWsQkLw50ZLm7eJkYPm1I7Fh/2AROb/e8K18P1319Z9N/0Q552xyumvpKsEBAQEBAQV+8WGjZbuKxut6kRccaTTtkDniF5BY4/YH8j+muU8T6Sb4q6isduk/Jt6XJyzNZVj8lw/dYLLeHLeLH/AJLg2/1NdZXsVf5PGTyu6WTRk8iEuPYOaezefMcDzHfteAcYxzSNLmnaY7fFX58U83NELDdQ8ie665qHI+IQOpvxTca+7cPQTXOY/WuCa5pLSDkIuQR5j6S1J1+ljvkr9YeuS0+Tu4rVOms7M+vhNQY3ISxt63sq2mSua3njkhpPA59V7xarDnnlxXiflO5NbV7w7GXxVDOYu3hsrVZapXoJK1mB45bLE9pa9p+wgkKeY3jaWImazFo7w85dAa2zvgD32zG0+um2be22o5xaq2ywvMcB+bHcY3j5xa3iKdg7noDhzw0O0on2F+Se0unzYo4xp65sX8SveP8APyfDxXeC0RQT72eHuvDmNLZKP8o28TjSJTXa8dRsVA3n2kB56ixvdnJ6QW9m4z4Ij71HrhnFYp/4+q6W9f3UpBBHIPK03SA80YfSOR8MrLEMro5YnBzJGO6XMcPItI7g/aFmJmOxMbxtK4Hhn+UE1ht/aqaQ3js2tS6ZJbFFk3kyZDHN8gXHzsxj1Dv5QDyLuOlbWLUzXatlDxDgmPNE5NP0t6eUpg+UC2q0/uttPi/EJoKetkZsFAx89um8PZdxEp+nyPpeye4OHwa6TnyUuoxxenPVocF1NtLnnS5em/5SwPyWOhLsNfXG5FmF7a1l9bC1HkdnmPqlm4P2F8Q+9Y0ldomyXxFnra1MdfnK9WV1PpzBSRw5rPY+g+UFzG2bLIi4DsSA4jkKXLqcOHaMloj5zs5uKzbtDo/xhaDJAGtMESTx/pCL8Si/1DS77e0r9YevZ3jvDPh7XDkOBH3rb3eHPI+IWQ6h5AoIB8RO8mPrYq1oDTVxti7bHsshPC/lteL60XUOxe7yIH0Rzz3K5Xj3F6Y8c6bDO8z3+Eejb0+GZnmsrD3+C4ZYd1lPCdo2WGtk9c24y1tr/IKfI82NPMrh9nUGt/oldn4X0c1rbU28+kNDVZN55IWJXYNMQEBAQEBB1cpjKWZx1nFZKsyxVtxuhmieOQ9jhwQVHkx1y0ml43ie7MTMTvCle7W0WZ2zyj5AySzhJ5D7pcA5DQfKOT+a8fsd5j1A+b8V4Tk4dfeOtJ7LPFnjJHWeqPxyfMdlURvum6NtxG6+5OBrtqYvWmSihb2ZG+QStaPgBIHcBb+DimswV5aZJiP89Uc4cc94ZA777ufV1vbP/Bg/Apo45xD+pP0j9mPd8fo+VnfDdm3XkrTa2u+zlaWODWRMJB8+C1gI+8HlYnjWvtG05J/I9hj9GjcgnuB+xVnNNvxJY6Jz8JHH8OMx2H+if+cxdT4WiK6m8R/8/wDMNXWfhj5rWLuVejDxAbA6M8Qein6U1TG6C3A502LykLAZ8fYI4628/SaeAHsPZw+BAIiyYq3jq2tHq76PJz0+nq8/9Pbi+I3wB6xOh9VY38saRtTukgqTPf7hcaD86WjPwTBIR3dGR5n5zPrLV5rYPu27OmyYdJxnH7XHO1/P/tIGe0X4OvGNK7OaB1dFttuHe5knoXI2QMtzEd+uAuEUrif9ZA8OPm4E9knkzfh6S1sWTW8L6Za89IQNuJ4HvEft5YmP8BZdSUGEll7AP97Y9vx9l2lb+tn61FbT3r2jda4eLaTPG/NtPpKE8lp/P4ad1XMYHJ0JmfSjtUpYXD7w5oKimlo7w365KXjesxP93V92stj9s6tMIh/rDG4N/rccLExMd3rePVb/AOT83P1La1Jk/D3l8Pez+jNU0rRmijYZG4lzmESSu9GQyAlrgfrlhHcu52tNO+9Z7KHjmDHFI1NJ5bxt/d6J7WbaaX2h0HiNvdH1nw4zEQCJjpD1SzPJ5fLI76z3uJcT8T8OFu0rFI5YcrnzX1GScl+8oG8XIH8J9P8AIB/6Pl8//dC4vxXbly4/lP6tnRxvWUC9LT9UfsXJ7zPVvTEN5q727rU60VStre82KFgYwOjieQB5DlzCT+sq0pxrX1rFYyT0+SC2DHPXZ9P4993f04uf/BB//Nev9d4h/U/KP2I0+PbeYY/L7tbmZys+pkta5OWF44dGx4iDh6g+zDeVDk4trssbXyTszXFSO1Wpt7nsPMqun70vct82r2pze5mXbHBHJWxFd498vFvAaPVkf86Qj08h5n4G14ZwvJxDJ22pHef2+KPLljFX4rqYXD4/AYurhsVWbXqU4mwwxt8mtH959SfUr6Vhx0w44x0jaI6Qq7Wm080u8pWBAQEBAQEBB171CnkqstK/Vis1529EsUrA9j2/Ag9iF4yY65KzW0bxJvt2Q9qfwsaFy8zrOBuXcHI5xJjiImh/Ux/dv6ncfYuf1PhvS5Z5sW9Z+sNimotXv1ahL4QskXER65rFoPYuoOB/+6r58Jz/AFPyTe+fB+B4QssPLW9P9xf+Nefspkj/AHI+jHvfwPzQsv8ApvT/AHF/41mPCmSO+SPoz758D80LLfpvT/cX/jT7KX36ZI+h758G+7ObHXtrs/dzNnUMGQbbp+6iOOuYy09bXc8lx5+jwrXhPBr8Oy2yWvvvG3ZDmz+1iI2S8r9riDBay0NpLcHT9nS2tdP0szirg4mq24w9hPo4erXD0c0hw9CvNqxaNpe8WW+C3PjnaVHd3PkvYrE0+V2Z1myuwkvZhs6HPYw+fTHZYC7j4dbHH4uWtbS+dZdFpfEE16aiu/xhEdfZz5QPZt/5O0vFrdlNhBY3CZht6q4Dy4j6zx9xYPuUU48tPwzLfnVcL1Ub323+MbSzEGo/lMc6PyXHU3DjDgYy+XEVavby7ySRt/bykTqNtuqHk4Rj+9vH1mWy6V8BXiN3byEGU8Qe5U+OpMeHurvyDslcI9QxoPsIifLnl3HwKV0179bSiy8Y02njk0td/wAoXh2h2R252P03/Brb3AsoxSFr7VmR3tLVyQDgPmlPd58+B2aOewAW5XHFI2hz2p1eXV358s7t9UjXRNvJsnd3SyuOyVbUEGPFGu+AskrukLy5/VzyHDhUPF+DW4netovttHo2MGf2O/RHv5oWX/Tin+4v/GqiPCmT+rH0S+9/A/NCy36cVP3F/wCNPspk/qx9D3uPQ/NCy36cU/3F/wCNPspk7e1j6HvnrD9R+EHJFwEuuawb6ltBxP8Aa9ZjwpftOWPoz75tHSG3aY8K2h8RM2xnr93NvYeRFIRBCfvaz5x/W5WGl8NaXB97JM2n6Qhtqb27dEw0MdRxVOKhjKcNWtC3ojhhYGMY34ADsF0GPHXHWKVjaIa9pmersjyXscrIICAgICAgICAgICAg4J4HKDq4rLYrOUIsphclVv05+r2dirM2WJ/Di09L2kg8EEHg+YIQdtAQEBB+ehvPPA5WNhyWtPm0LIAAeSDlAQEBAQEBAQEBAQEBAQEBAQY/P5aHA4S/nLLHvhx9aW1I1n0nNY0uIH2nhRZssYcdsk+Ubs1jmnZruloNf3Y8fqDM6kpiK6xs8+LjoDohY9vLWMl6usubyOXO5B4PYdlq4PeLcuS946+W36T3ercsdNmy5fLY/A4m5m8vbjq0cfXktWp5DwyKGNpc95PoA0E/qW+8K+eFffHcrcHO5zTu7talTv5fF0NeaVggr+xezT2QdI2KvIPrzV3RNbI7z5nZyEG+eKbcDUm1fh619uJo+xBBmcBhJ7tKWaESsZK3jhxYezgOT2PZB0PDvqyLVlTM2oPErgd22QvrtL8VTpVxjHOa49D/AHVzuS8dx19x0Hj1Qaf4xtx8ppGfb3Rf8Y2P0Rp/XeUyWIz2YuY9ltsdVuNnlbGGvI6faPa1nUCCOvsQglPYarj6e1Gnq+G1njdU4xlcihksbioMbVkrB7hGyKvABHGxjQGANA+jyg389gghKlmN2t2dSavfozXlLR2D0tmJdP0m/kaO/PftQMYZ5pzK4BsIe/oayPpcQxzi8cgCOJm2+zcmmPBWvPXmmY377bOzg93dTZzw66j3GnpU6ep9O47OQ2oYgZKwyOO9vG5zATyYnSQhwaTzw7gk8crNJ5o3R6rDGDJNI7dJ+sbtT8Iu5uS3Ow9HO5fxOad3CyVzTtDIZHT2Mx9GvLhrM7GPeZDXeZOzi+PpeB3HfuF7a6yKDQ8td1dlNf29OYTUkWKq08TWugOoMse0kkllYerqIIADB2BCrb3z5NTbFjvtERE9t0kREV3mGY0JqS/qPFWPyvWhgyWNvT424ICTE6WJ3HWznv0uaWuAPcc8ein0ee2es83eJmJ/s83rFZ6K7eI3fPUOit9sfoCbf7Tm1GmxoezqN2Ry+KrWxbvMvNgbD/LPaSOgk9EZD3cHj7Nt5TdsLrHWuv8AZ3SOstxdNDT+pMxiobeSxwjfGIJnDvwyT58YcOHhjvnNDuk9wUEGbvb0aix/iIzm2VnxIYra3FY/TmFv42Ozgat6TI2bc92OXh03cBnsIQAP56C1lVk8NaOOzY9vKxjQ+XpDetwHd3A7Dk9+PTlBWrUviF1zi/EpTxFX3QbV0M5R0BmZjX6pv4RXqj7UMjZvSOMmlWc3+fb79wAAs1zyOfLlBV7w6eIfXeqt0tfaH3VvY1tAZHOXdI24qzazRj8ZlJqFyvK7nh74S2rL1HuWzknsEHZ8J+/O429G4W438LDUh02ynhM/pGpHUEU0GKvm57B0z+eXvlhrwzd/L2vA7INr8Vm5Osds9J6eymn8ydNYW9noqWpdVjE/lM6exxhld7z7v9Hh0zIYjK8OZEJS5zTwg37aa7dyegcVkbu42L14LMbpYNRYytFBXyEDnuMbw2J74+Q3hrnMIaS0kNbz0gNwQEBAQEBAQdPL4yrmsXbxF5hdWvQSVpmg8Ese0tdx+oqPLjjLSaW7T0Zidp3azprC7g4f3HEX81hrOLx7WxCw2tKLdiJrelgcC7oY7sOXDnnjsBytTBj1OOK47Wiax59d5j9GZ2nqx2/e3Ob3a2rzO2uGzcWJZqMQY/JWnhxd+TXzM99jj6fKSSuJY2k9gX8nyW9G/m8tJo+FbT2h91tEbnbV3ruLfgYruJzFXJZi/kW3cPYhHFeI2JZPZGOeKvI0N4aQ1wPpxkbp4htsshvLsprHa3F5OtjrepsVLj4rVhjnxROdx85zW9yO3og3rH4+rQgbHXrQxHpaHGOMM6iBx6IIo332w3E1lqbbvXW2WV05WzOgstdyLYM9HYdWsts0JqhaTAQ8ECYuHoeBz8EEgaEGvG6crDcl2Bdn+qT3k4MTin09Z6Oj25MnPTxzz688dkGwkcjhBDT9uN29Eam1Rd2n1FpY4fV2RdmZqufq2HSY2/JGxk0kLoHASxvMbX+zf08O6uHcO7R8tomeVtTnx5K1jLE716dPOGWo7MtwexOY2hxmcdZuZfFZSvYytqPgz3rwldNZexvkDLM53SD2bwAeyzSOWNkefNOfJN5bDtPodu3e2+lNFzOqz28BgsfibFqvD7Ns769dkTngHvwSwkA9+69oW3oNKzGm9Xx6zn1Ppm5hmMt42ChKy/FK8sMckjw5oYQD/nPIkeSrsmDPGe2XFMdYiOu/l8nvmiaxWWZ0jpoaXxRpyXn3bVixNcuWnsDDNYlcXPd0js0cngNHkAB3WzpsHu9OXfeZneZ+MsXtzzuj7U2xNbWG+VjcbUbsbkdOXtAWtF3cPYrl7p/bXmWHPJPzejoaW8efJBWw8s/sXofVW2m1eA0BrDUseoL2nq7sdFk2tc11ipFI5tUydXcyiARNeeSC5riPNBourdtd98RvVqLdPaLK6CdBqXAYnD2a2o4rxfC+jLce1zDXIBa73w889/m9vXkJwq+9mpH757I2fZt9r7PkM6+PndPPfp554578IKr2fAnhs3tFncTqDU99+5eetXdQzahq5fIx0Y8/LZNmC02iJhCWQyNgaAY+S2IevdBaTEtyTcVUbmX135AQR+9urAiIzdI6ywO79PVzxz344QVa1p4Ls5qzQseAp6/iw+abrjUWadk6kL+p2Czdif3/AB/B7h768wb1DsJImOHHAICX9vdnDoPd7XOvKNmlFhtS4fT2Jx2NrxOa6kzGx2WEEn5paROwNA8g08oM7ubX3dkp0Z9o72km2opne+1NR17LoLMJb2DJa7uqJ7XAHux4cCRwPNBgvDrtBd2Z0PewWVydC3ks1ncjqK9HjKrquOqT3JjI6vThcXOjgZ2DQTyT1OPBdwAlJAQEBAQEBAQEBAQEBAQEBBwgEc+qMTG7lGRAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQf/Z"
                className='rounded'
                      height='150'
                      width='150'
                      loading='lazy'
                 />Cafè  of Dreams</small>
                    </p>
                  </MDBCol>

                  <MDBCol md="2" lg="2" xl="2" className='mx-auto mb-4'>
                    <h6 className='text-uppercase fw-bold mb-4'>Products</h6>
                    <p>
                      <a href='#!' className='text-reset'>
                        Hot Coffee
                      </a>
                    </p>
                    <p>
                      <a href='#!' className='text-reset'>
                        Iced Coffee
                      </a>
                    </p>
                    <p>
                      <a href='#!' className='text-reset'>
                        Frappuccino & Smoothies
                      </a>
                    </p>
                    <p>
                      <a href='#!' className='text-reset'>
                        Snacks
                      </a>
                    </p>
                  </MDBCol>

                  <MDBCol md="3" lg="2" xl="2" className='mx-auto mb-4'>
                    <h6 className='text-uppercase fw-bold mb-4'>Useful links</h6>
                    <p>
                      <a href='#!' className='text-reset'>
                        Product List
                      </a>
                    </p>
                  
                    <p>
                      <a href='#!' className='text-reset'>
                        Orders
                      </a>
                    </p>
                    <p>
                      <a href='#!' className='text-reset'>
                        Help
                      </a>
                    </p>
                  </MDBCol>

                  <MDBCol md="4" lg="3" xl="3" className='mx-auto mb-md-0 mb-4'>
                    <h6 className='text-uppercase fw-bold mb-4'>Contact</h6>
                    <p>
                      <MDBIcon icon="home" className="me-2" />
                      Dasmarinas, Cavite, 4115, Ph
                    </p>
                    <p>
                      <MDBIcon icon="envelope" className="me-3" />
                      CafeOfDreams@mail.com
                    </p>
                    <p>
                      <MDBIcon icon="phone" className="me-3" /> + 63 987654321
                    </p>
                    <p>
                      <MDBIcon icon="print" className="me-3" /> + 63 912345678
                    </p>
                  </MDBCol>
                </MDBRow>
              </MDBContainer>
            </section>


      <div className='text-center p-3' style={{ backgroundColor: 'rgba(0, 0, 0, 0.2)' }}>
        © 2023 Copyright:
        <a className='text-white' href='https://mdbootstrap.com/'>
          CafeOfDreams.com
        </a>
      </div>

      
    </MDBFooter>


	)


}