import React, {  useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, Link } from 'react-router-dom';
// import Swal from 'sweetalert2';
// import UserContext from '../UserContext';
import SidebarMenu from 'react-bootstrap-sidebar-menu';
import { 
  MDBContainer,
  MDBNavbar,
  MDBCard,
  MDBCardBody,
  MDBNavbarBrand,
  MDBRipple,
  MDBIcon,
  MDBNavbarNav,
  MDBNavbarItem,
  MDBBtn,
  MDBDropdown,
  MDBDropdownToggle,
  MDBDropdownMenu,
  MDBDropdownItem,
  MDBRow,
  MDBCol,
  MDBBadge,
  MDBSideNav,
  MDBSideNavMenu,
  MDBSideNavItem,
  MDBSideNavLink,
  MDBSideNavCollapse
} from 'mdb-react-ui-kit';


export default function AfterLoginBanner() {

        // // Allows us to consume the User context object and its properties to use for user validation 
        // const { user, setUser} = useContext(UserContext);

        // // State hooks to store the values of the input fields
        // const [email, setEmail] = useState('');
        // const [password, setPassword] = useState('');
        // // State to determine whether submit button is enabled or not
        // const [isActive, setIsActive] = useState(true);


        // const retrieveUserDetails = (token) => {

        //     // The token will be sent as part of the request's header information
        //     // We put "Bearer" in front of the token to follow implementation standards for JWTs
        //     fetch("http://localhost:4000/users/details", {
        //         headers: {
        //             Authorization: `Bearer ${token}`
        //         }
        //     })
        //     .then(res => res.json())
        //     .then(data => {
        //         console.log(data);

        //         // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
        //         setUser({
        //             id: data._id,
        //             isAdmin: data.isAdmin
        //         });
        //     });
        // };


        // function authenticate(e) {

        //     // Prevents page redirection via form submission
        //     e.preventDefault();

        //     fetch('http://localhost:4000/users/login', {
        //         method: "POST",
        //         headers: {
        //             'Content-Type': 'application/json'
        //         },
        //         body: JSON.stringify({
        //             email: email,
        //             password: password
        //         })
        //     })
        //     .then(res => res.json())
        //     .then(data => {

        //         console.log(data);

        //         // If no user information is found, the "accessToken" property will not be available and will return undefined
        //         if(data.accessToken !== undefined) {

        //             // The JWT will be used to retrieve user information across the the whole frontend application and storing it in the localStorage will allow ease of access to the user's information
        //             localStorage.setItem('token', data.accessToken);
        //             retrieveUserDetails(data.accessToken);

        //             Swal.fire({
        //                 title: "Login Successful",
        //                 icon: "success",
        //                 text: "Welcome!"
        //             })
        //         } else {
        //             Swal.fire({
        //                 title: "Authentication failed!",
        //                 icon: "error",
        //                 text: "Check your log in credentials and try again!"
        //             })
        //         }
        //     })

            
        //     setEmail('');
        //     setPassword('');

        // }


        // useEffect(() => {

        //     // Validation to enable submit button when all fields are populated and both passwords match
        //     if(email !== '' && password !== ''){
        //         setIsActive(true);
        //     }else{
        //         setIsActive(false);
        //     }

        // }, [email, password]);

	const [showV1, setShowV1] = useState(true);
	  const [showV1Backdrop, setShowV1Backdrop] = useState(false);
	    const [collapseOpened, setCollapseOpened] = useState('accordionCollapse1');

	  const toggleAccordion = (value: string) => {
	    value !== collapseOpened ? setCollapseOpened(value) : setCollapseOpened('');
	  };


	return (
     

		<MDBContainer fluid>
      <MDBRow className='justify-content-center'>
        <MDBCol className="p-0">
          <header style={{ position: 'relative', overflow: 'hidden', minHeight: '400px' }}>
            

           
            <section className='text-center text-md-start'>
              <div
                className='p-5 bg-image'
                style={{
                  height: '350px',
                  backgroundImage: 'url("https://i.postimg.cc/BnhcVDpX/7.jpg")',
                }}
              ></div>

              <MDBCard
                className='mx-4 mx-md-5 shadow-5-strong'
                style={{ marginTop: '-50px', background: 'hsla(0, 0%, 100%, 0.7)', backdropFilter: 'blur(30px)' }}
              >
                <MDBCardBody className='px-5 px-md-5'>
                  <MDBRow className='d-flex justify-content-center'>
                    <MDBCol lg='10'>
                      <MDBRow className='gx-lg-4 align-items-center'>
                        <MDBCol lg='6' className='mb-4 mb-lg-0 text-center text-lg-start'>
                          <h1>Cafè  of Dreams</h1>
                          
                        </MDBCol>
                        <MDBCol lg='6' className='text-center text-lg-end'>
                          <Button variant='light' className='btn btn-link me-2' size='lg' as={ Link} to="/contact">
                            See Our Location
                          </Button>
                          <Button size='lg' as={ Link} to="/products" >Order Now</Button>
                        </MDBCol>
                      </MDBRow>
                    </MDBCol>
                  </MDBRow>
                </MDBCardBody>
              </MDBCard>
            </section>
          </header>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
	)
}