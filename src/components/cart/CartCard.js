
import { Row, Col, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CartCard({cartProp}) {

  console.log(cartProp);

  // Deconstructs the cart properties into their own variables
  const {_id, userId, date, products} = cartProp;

   
    return (
       
            <Col>
                <Card>
                    <Card.Body className="mb-3">

                        <Card.Subtitle>UserId:</Card.Subtitle>
                        <Card.Text>{userId}</Card.Text>
                        <Card.Subtitle>Date:</Card.Subtitle>
                        <Card.Text>{date}</Card.Text>
                        <Card.Subtitle>Product Name: </Card.Subtitle>
                        <Card.Text>{products.map((product) => product.productName)}</Card.Text>
                        <Card.Subtitle>Product Quantity: </Card.Subtitle>
                        <Card.Text>{products.map((product) => product.quantity)}</Card.Text>
                        
                        
                       
                        
                    </Card.Body>
                </Card>
            </Col>

        
    )
}


