// [SECTION] Creating React Application:
	// Syntax:
		npx create-react-app <project-name>

// Delete unecesary files from the app
	// application > src
	App.test.js
	index.css
	logo.svg
	reportWebVitals.js

// Remove the importation of the "index.css" and "reportWebVitals" files from the "index.js" file. Also remove the code using the reportWebVitals function.
// Application > src > index.js

// Remove the importation of the "logo.svg" file and most of the codes found inside the "App" component to remove any errors.
// Application > src > App.js

// [SECTION] React JSX
/*
	The syntax used in Reactjs is JSX.

		- JSX - Javascript + XML, It is an extension of Javscript that let's us create objects which will then be compiled and added as HTML elements.

		- With JSX, we are able to create HTML elements using JS.

		- With JSX, we are able to create JS objects that will then be compiled and added as HTML elements.

*/

// [SECTION] ReactJS Component

/*
		- These are reusable parts of our react application.
		- They are independent UI parts of our app.
		- Components are functions that return react elements.
		- Components naming Convention: PascalCase
			- Capitalized letter for all words of the function name AND file name associated with it.
*/

/*
	Syntax:
		import { moduleName/s } from "file path"

*/

// [SECTION] React import pattern:

/*
		-imports from built-in react modules.
		-imports from downloaded packages
		-imports from user defined components
*/

// [SECTION] Props and State Hooks

/*
	Props
		- is a shorthand for "property" since components are considered as object in ReactJS
		- is a way to pass data from a parent component to a child component.
		- it is synonymous to function parameters.
		- it is used like an HTML attribute added to the child component.

*/

/*

	States
	- States are a way to store information within a component. This information can then be updated within the component. 
	- States are used to keep track of information related to individual components.

	Hooks 
	- Special/react-defined methods and functions that allow us to do certain tasks in our components.
		- useState()
	
*/

// [SECTION] Effect Hooks

/*
	// Effect hooks in React allow us to execute a piece of code whenever a component gets rendered to the page or if the value of a state changes.

	//useEffect() allows us to perform "side effects" in our components or run a specifc task. 
		//Some examples of side effects are: fetching data, directly updating the DOM, and timers. 

	// Syntax:
		//useEffect(function, [dependency])

	//useEffect() always runs the task on the initial render and/or every render (when a state changes in a component).
		//Initial render is when the component is run or displayed for the first time.

	// No dependecy array passed
		// If the useEffect() does not have a dependency array, it will run on initial render and whenever a state is set by its function.
	// useEffect(()=>{
	// 	console.log("Will run on initial render or on every changes with our components");
	// });

	// An empty array
		// If the useEffect() has dependency array but it is empty, it will only run on the initial render.
	// useEffect(()=>{
	// 	console.log("Will only run on initial render.");
	// }, []);

	// With dependency array (props or state values);
		// if the useEffect() has a dependency array and there is state or data in it, the useEffect will run whenever that state is updated.
	// useEffect(()=>{
	// 	console.log("Will run on initial render and every change on the dependency value.");
	// }, [seats]);

*/

// [SECTION] React Context API
	
	// React Context API (useContext) - provides a way to share values (state) between components without having to explicitly pass a "prop" through each component.

	// React Context
		// Allows us to pass down and use (consume) data in any component we need in our React application without using "props".
		//  In other words, React context allows us to share data (state, function, etc.) across components more easily.

		// 3 simple steps in using react context.
			// 1. Creating the context
			// 2. Providing the context
			// 3. Consuming the context



// Documentations:
/*
	Virtual DOM
		https://www.youtube.com/watch?v=M-Aw4p0pWwg
	React Tutorial
		https://www.w3schools.com/react/default.asp
	Built-in React Components
		https://react.dev/reference/react/components
	React-Bootstrap Documentation
		https://react-bootstrap.github.io/
	React JS States
		https://reactjs.org/docs/hooks-state.html
		https://www.w3schools.com/react/react_usestate.asp
	React JS useEffect Hook
		https://reactjs.org/docs/hooks-effect.html#gatsby-focus-wrapper
	React JS Synthetic Events
		https://reactjs.org/docs/events.html
*/

// Packages installed:
/*
	1. bootstrap
	2. react-bootstrap
	3. react-router-dom
	4. sweetalert2
	
*/